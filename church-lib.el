(defun eden-get-files-under (DIRECTORY EXTENSION &optional FOLLOW-SYMLINKS)
  "Returns a list of all files under given DIRECTORY with the
extension EXTENSION. Will follow symlinks if FOLLOW-SYMLINKS is
non-nil, defaults to nil."
  (let ((REGEXP (format "\\.%s$" EXTENSION)))
    (directory-files-recursively DIRECTORY REGEXP nil nil FOLLOW-SYMLINKS)))

(defun eden-get-files-regex (DIRECTORY REGEXP &optional FOLLOW-SYMLINKS)
  "Returns a list of all files under given DIRECTORY matching
REGEXP. Will follow symlinks if FOLLOW-SYMLINKS is non-nil,
defaults to nil."
  (directory-files-recursively DIRECTORY REGEXP nil nil FOLLOW-SYMLINKS))

(provide 'church-lib)
