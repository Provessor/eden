(add-hook 'tty-setup-hook #'xterm-mouse-mode)

(defun eden-fix-shell-hscroll-h ()
  (setq hscroll-margin 0))
(add-hook 'eshell-mode-hook #'eden-fix-shell-hscroll-h)
(add-hook 'term-mode-hook #'eden-fix-shell-hscroll-h)

(when IS-MAC
  (and (or (daemonp)
	   (display-graphic-p))
       (require 'ns-auto-titlebar nil t)
       (ns-auto-titlebar-mode +1)))

(when IS-MAC
  (defun eden-init-menu-bar-in-gui-frames-h (&optional frame)
    "Re-enable menu-bar-lines in GUI frames."
    (when-let (frame (or frame (selected-frame)))
      (when (display-graphic-p frame)
        (set-frame-parameter frame 'menu-bar-lines 1))))
  (add-hook 'window-setup-hook #'eden-init-menu-bar-in-gui-frames-h))

(setq window-divider-default-places t
      window-divider-default-bottom-width 1
      window-divider-default-right-width 1)
(add-hook 'emacs-startup-hook #'window-divider-mode)

(setq split-width-threshold 160
      split-height-threshold nil)

(setq window-resize-pixelwise t
      frame-resize-pixelwise t)

(setq minibuffer-prompt-properties '(read-only t intangible t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

(setq enable-recursive-minibuffers t)

(setq frame-title-format '("%b – Eden Emacs")
      icon-title-format frame-title-format)

(setq x-underline-at-descent-line t)

(provide 'church-ui)
