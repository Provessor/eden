(advice-add #'package--ensure-init-file :override #'ignore)
(advice-add #'package--save-selected-packages :override #'ignore)

(setq straight-base-dir eden-local-dir
      straight-repository-branch "develop"
      straight-enable-package-integration nil
      straight-cache-autoloads t)



(defun eden-ensure-straight ()
  "Ensure `straight' is installed."
  (defvar bootstrap-version)
  (let ((user-emacs-directory straight-base-dir)
        (bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el"
                           straight-base-dir))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (make-directory (expand-file-name "straight/build"
                                        straight-base-dir) 'parents)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(defun eden--straight-normalizer
    (name-symbol _keyword args)
  "Normalizer for `:straight' in `use-package' forms.
NAME-SYMBOL, KEYWORD, and ARGS are explained by the `use-package'
documentation."
  (let ((parsed-args nil))
    (dolist (arg args)
      (cond
       ((null arg) (setq parsed-args nil))
       ((eq arg t) (push name-symbol parsed-args))
       ((symbolp arg) (push arg parsed-args))
       ((not (listp arg))
	(use-package-error ":straight wants a symbol or list"))
       ((keywordp (car arg))
	;; recipe without package name
	(push (cons name-symbol arg) parsed-args))
       ((cl-some #'keywordp arg)
	;; assume it's a recipe
	(push arg parsed-args))
       (t
	(setq parsed-args
	      (append (straight-use-package--straight-normalizer
		       name-symbol nil arg)
		      parsed-args)))))
    parsed-args))

(defun eden--straight-handler
    (name _keyword args rest state)
  "Handler for `:straight' in `use-package' forms.
NAME, KEYWORD, ARGS, REST, and STATE are explained by the
`use-package' documentation."
  nil)

(defvar eden--package-initialized-p nil
  "Whether or not straight has been initialized")
(dolist (package (file-expand-wildcards (concat eden-local-dir "straight/build/*")))
  (add-to-list 'load-path package))
(dolist (package-autoloads-file (file-expand-wildcards (concat eden-local-dir "straight/build/*/*-autoloads.el")))
  (when (file-exists-p package-autoloads-file)
    (load package-autoloads-file nil 'nomessage)))

(defun eden-initialize-packages ()
  ""
  (eden-ensure-straight)
  (setq eden--package-initialized-p t))

(defun eden-load-packages ()
  ""
  (with-eval-after-load 'use-package-core
    (when (and (boundp 'use-package-keywords)
	       (listp use-package-keywords))
      (push :straight use-package-keywords))
    (defalias 'use-package-normalize/:straight
      #'eden--straight-normalizer)
    (defalias 'use-package-handler/:straight
      #'eden--straight-handler)))

(defun eden-require (FEATURE &optional PACKAGE)
  "Require FEATURE and ensure it's installed if the packaging
system has been initialized. If PACKAGE is given, that is
installed instead."
  (if noninteractive
      (if PACKAGE
	  (straight-use-package PACKAGE)
	(straight-use-package FEATURE))
    (require FEATURE)))

(provide 'church-package)
