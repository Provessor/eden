(defun eden-load-module (MODULE)
  "Load given MODULE"
  (load MODULE))

(defun eden-load-modules (MLIST)
  "Loads modules in MLIST"
  (dolist (module MLIST)
    (load module)))

(defun eden-load-modules-all ()
  "Loads all modules"
  (dolist (module (eden-get-files-under eden-module-dir "el" t))
    (load (file-name-base module))))

(provide 'church-modules)
