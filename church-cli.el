(require 'org)

(defun eden-tangle-file (FILE)
  "Extract bodies of emacs-lisp source code blocks in
FILE. Source code blocks are extracted with
`org-babel-tangle-file' which is a little wrapper around
`org-babel-tangle'. Returns a list whose CAR is the tangled file
name."
  (let ((TARGET-FILE (format "%s.el" (file-name-sans-extension FILE)))
        (LANG 'emacs-lisp))
    (org-babel-tangle-file FILE TARGET-FILE LANG)))

(defun eden-tangle-church ()
  "Tangle all eden files with `eden-tangle-file' and
`eden-get-files-under'"
  (dolist (file (eden-get-files-under
                 (expand-file-name "eden" user-emacs-directory) "org" t))
    (eden-tangle-file file)))

(defun eden-tangle-all ()
  "Tangle all files under `user-emacs-directory' with
`eden-tangle-file' and `eden-get-files-under'"
  (dolist (file (eden-get-files-under user-emacs-directory
                                      "org" t))
    (eden-tangle-file file)))

(defun eden-byte-compile-file (FILENAME)
  "Compile a file of Lisp code named FILENAME into a file of byte
code. Does not load the file after compiling. The output file's
name is chosen by `byte-compile-file'"
  (byte-compile-file FILENAME nil))

(defun eden-byte-compile-church ()
  "Tangle all eden files with `eden-byte-compile-file' and
`eden-get-files-under'"
  (dolist (file (eden-get-files-under
                 (expand-file-name "eden" user-emacs-directory) "el" t))
    (eden-byte-compile-file file)))

(defun eden-byte-compile-all ()
  "Tangle all files under `user-emacs-directory' with
`eden-byte-compile-file' and `eden-get-files-under'"
  (dolist (file (eden-get-files-under user-emacs-directory
                                      "el" t))
    (eden-byte-compile-file file)))

(defun eden-update ()
  "Ensures all packages are updated to match the version lock and
any local changes have been rebuilt."
  (straight-check-all))

(defun eden-upgrade ()
  "Remove the version lock, update all straight packages and then
reapply the version lock"
  (straight-thaw-versions)
  (straight-pull-all)
  (straight-freeze-versions))

(defun eden-update-package (PACKAGE)
  "Ensures package PACKAGE is updated to match the version lock
and any local changes have been rebuilt."
  (straight-check-package PACKAGE))

(defun eden-upgrade-package (PACKAGE)
  "Remove the version lock, update package PACKAGE and then
reapply the version lock"
  (straight-thaw-versions)
  (straight-pull-package PACKAGE)
  (straight-freeze-versions))

(defun eden-sync-church ()
  "Tangle and byte-compile all eden files"
  (eden-tangle-church)
  (eden-byte-compile-church))

(defun eden-sync-all ()
  "Tangle and byte-compile all files under
`user-emacs-directory'"
  (eden-tangle-all)
  (eden-byte-compile-all))

(provide 'church-cli)
