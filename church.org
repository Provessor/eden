#+TITLE: The Nave of The Church

* Compatibility
Check the Emacs version and whether it matches our minimum supported version
#+begin_src emacs-lisp
  (when (version< emacs-version "27.0.90")
    (error "Detected Emacs %s. Eden only supports Emacs 27.0.90 and higher"
           emacs-version))
#+end_src

Setup for system checks
#+begin_src emacs-lisp
  (defconst IS-MAC     (eq system-type 'darwin))
  (defconst IS-LINUX   (eq system-type 'gnu/linux))
  (defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))
  (defconst IS-BSD     (or IS-MAC (eq system-type 'berkeley-unix)))
#+end_src

Warn when using ~customize~
#+begin_src emacs-lisp
  (dolist (sym '(customize-option customize-browse customize-group customize-face
                 customize-rogue customize-saved customize-apropos
                 customize-changed customize-unsaved customize-variable
                 customize-set-value customize-customized customize-set-variable
                 customize-apropos-faces customize-save-variable
                 customize-apropos-groups customize-apropos-options
                 customize-changed-options customize-save-customized))
    (put sym 'disabled "Eden doesn't support `customize', configure Emacs from $EMACSDIR/config.el instead"))
  (put 'customize-themes 'disabled "Set `load-theme' in $EMACSDIR/config.el instead")
#+end_src

* Loading
Ensure the church is in ~load-path~
#+begin_src emacs-lisp
  (add-to-list 'load-path (file-name-directory load-file-name))

  (defvar eden--initial-load-path load-path)
  (defvar eden--initial-process-environment process-environment)
  (defvar eden--initial-exec-path exec-path)
#+end_src

~file-name-handler-alist~ is consulted on every ~require~, ~load~ and various
path/io functions. You get a minor speed up by nooping this. However, this
may cause problems on builds of Emacs where its site lisp files aren't
byte-compiled and we're forced to load the *.el.gz files (e.g. on Alpine).

Also add a hook to restore ~file-name-handler-alist~ because it's needed for
handling encrypted or compressed files, among other things.
#+begin_src emacs-lisp
  (unless noninteractive
    (defvar eden--initial-file-name-handler-alist file-name-handler-alist)
    (setq file-name-handler-alist nil)

    ;; Restoration
    (defun eden-reset-file-handler-alist-h ()
      (setq file-name-handler-alist eden--initial-file-name-handler-alist))
    (add-hook 'emacs-startup-hook #'eden-reset-file-handler-alist-h))
#+end_src

* Runtime Setup
** Global Variables
*** Current Running Mode
#+begin_src emacs-lisp
          (defvar eden-debug-mode (or (getenv "DEBUG") init-file-debug)
            "If non-nil, Eden will log more.

          Use `eden/toggle-debug-mode' to toggle it. The --debug-init flag and setting the
          DEBUG envvar will enable this at startup.")

          (defvar eden-interactive-mode (not noninteractive)
            "If non-nil, Emacs is in interactive mode.")
#+end_src

*** Debug
Some custom error types
#+begin_src emacs-lisp
  (define-error 'eden-error "Error in Eden Emacs core")
  (define-error 'eden-hook-error "Error in a Eden startup hook" 'eden-error)
  (define-error 'eden-autoload-error "Error in an autoloads file" 'eden-error)
  (define-error 'eden-module-error "Error in a Eden module" 'eden-error)
  (define-error 'eden-private-error "Error in private config" 'eden-error)
  (define-error 'eden-package-error "Error with packages" 'eden-error)
#+end_src

Longer logs
#+begin_src emacs-lisp
  (setq message-log-max 8192)
#+end_src

Only show debug when we ask for it
#+begin_src emacs-lisp
  (setq debug-on-error eden-debug-mode
        jka-compr-verbose eden-debug-mode)
#+end_src

Disable warnings from legacy advice system. They aren't useful, and what can
we do about them, besides changing packages upstream?
#+begin_src emacs-lisp
  (setq ad-redefinition-action 'accept)
#+end_src

*** Directories/Files
#+begin_src emacs-lisp
  (defconst eden-emacs-dir
    (eval-when-compile (file-truename user-emacs-directory))
    "The path to the currently loaded Emacs configuration
    directory")

  (defconst eden-local-dir (concat eden-emacs-dir ".local/")
    "Root directory for local storage

  Use this as a storage location for this system's installation of Doom Emacs.
  These files should not be shared across systems. By default, it is used by
  `doom-etc-dir' and `doom-cache-dir'. Must end with a slash.")

  (defconst eden-etc-dir (concat eden-local-dir "etc/")
    "Directory for non-volatile local storage.

  Use this for files that don't change much, like server binaries, external
  dependencies or long-term shared data. Must end with a slash.")

  (defconst eden-var-dir (concat eden-local-dir "var/")
    "Directory for volatile local storage.

  Use this for files that change often, like cache files. Must end with a slash.")
#+end_src

*** Encoding
This is all that's really needed to make UTF-8 the default encoding system
#+begin_src emacs-lisp
  (when (fboundp 'set-charset-priority)
    (set-charset-priority 'unicode))
  (prefer-coding-system 'utf-8)
  (setq locale-coding-system 'utf-8)
#+end_src

The clipboard's on Windows could be in an encoding that's wider (or thinner)
than UTF-8, so let Emacs/the OS decide what encoding to use there.
#+begin_src emacs-lisp
  (unless IS-WINDOWS
    (setq selection-coding-system 'utf-8)) ; with sugar on top
#+end_src

*** Security
Emacs is essentially one huge security vulnerability, what with all the
dependencies it pulls in from all corners of the globe. Let's try to be at
least a little more discerning.
#+begin_src emacs-lisp
  (setq gnutls-verify-error (not (getenv "INSECURE"))
        gnutls-algorithm-priority
        (when (boundp 'libgnutls-version)
          (concat "SECURE128:+SECURE192:-VERS-ALL:+VERS-TLS1.2"
                  (if (and (not IS-WINDOWS)
                           (not (version< emacs-version "26.3"))
                           (>= libgnutls-version 30605))
                      ":+VERS-TLS1.3")))
        ;; `gnutls-min-prime-bits' is set based on recommendations from
        ;; https://www.keylength.com/en/4/
        gnutls-min-prime-bits 3072
        tls-checktrust gnutls-verify-error
        ;; Emacs is built with `gnutls' by default, so `tls-program' would not
        ;; be used in that case. Otherwiese, people have reasons to not go with
        ;; `gnutls', we use `openssl' instead.
        ;; For more details, see https://redd.it/8sykl1
        tls-program '("openssl s_client -connect %h:%p -CAfile %t -nbio -no_ssl3 -no_tls1 -no_tls1_1 -ign_eof"
                      "gnutls-cli -p %p --dh-bits=3072 --ocsp --x509cafile=%t \
  --strict-tofu --priority='SECURE192:+SECURE128:-VERS-ALL:+VERS-TLS1.2:+VERS-TLS1.3' %h"
                      ;; compatibility fallbacks
                      "gnutls-cli -p %p %h"))
#+end_src

Emacs stores authinfo in $HOME and in plaintext. Let's not do that, mkay?
This file stores usernames, passwords, and other such treasures for the
aspiring malicious third party.
#+begin_src emacs-lisp
  (setq auth-sources (list (expand-file-name "authinfo.gpg" eden-etc-dir)
                           "~/.authinfo.gpg"))
#+end_src

Don't ping things that look like domain names.
#+begin_src emacs-lisp
  (setq ffap-machine-p-known 'reject)
#+end_src

*** Little Quality of Life Improvements
Make apropos omnipotent. Now most apropos commands act as if they were given a
prefix argument. The one exception is ~apropos-variable~ which will always
search for all variables.
#+begin_src emacs-lisp
  (setq apropos-do-all t)
#+end_src

Get rid of "For information about GNU Emacs..." message at startup, unless
we're in a daemon session, where it'll say "Starting Emacs daemon." instead,
which isn't so bad.
#+begin_src emacs-lisp
  (unless (daemonp)
    (advice-add #'display-startup-echo-area-message :override #'ignore))
#+end_src

Emacs on Windows frequently confuses HOME (=C:\Users\<NAME>=) and =%APPDATA%=,
causing `abbreviate-home-dir' to produce incorrect paths.
#+begin_src emacs-lisp
  (when IS-WINDOWS
    (setq abbreviated-home-dir "\\`'"))
#+end_src

Delete files to trash on macOS, as an extra layer of precaution against
accidentally deleting wanted files.
#+begin_src emacs-lisp
  (setq delete-by-moving-to-trash IS-MAC)
#+end_src

** Optimisation
*** Startup
Remove command line options that aren't relevant to our current OS; means
slightly less to process at startup.
#+begin_src emacs-lisp
  (unless IS-MAC   (setq command-line-ns-option-alist nil))
  (unless IS-LINUX (setq command-line-x-option-alist nil))
#+end_src

*** Runtime
A second, case-insensitive pass over ~auto-mode-alist~ is time wasted, and
indicates misconfiguration (or that the user needs to stop relying on case
insensitivity).
#+begin_src emacs-lisp
  (setq auto-mode-case-fold nil)
#+end_src

Emacs "updates" its ui more often than it needs to, so we slow it down slightly
from 0.5s:
#+begin_src emacs-lisp
  (setq idle-update-delay 1)
#+end_src

Disable bidirectional text rendering for a modest performance boost. I've set
this to ~nil~ in the past, but the ~bidi-display-reordering~'s docs say that is
an undefined state and suggest this to be just as good:
#+begin_src emacs-lisp
  (setq-default bidi-display-reordering 'left-to-right
                bidi-paragraph-direction 'left-to-right)
#+end_src

Reduce rendering/line scan work for Emacs by not rendering cursors or regions in
non-focused windows.
#+begin_src emacs-lisp
  (setq-default cursor-in-non-selected-windows nil)
  (setq highlight-nonselected-windows nil)
#+end_src

More performant rapid scrolling over unfontified regions. May cause brief spells
of inaccurate syntax highlighting right after scrolling, which should quickly
self-correct.
#+begin_src emacs-lisp
  (setq fast-but-imprecise-scrolling t)
#+end_src

Font compacting can be terribly expensive, especially for rendering icon fonts
on Windows. Whether it has a noteable affect on Linux and Mac hasn't been
determined, but we inhibit it there anyway.
#+begin_src emacs-lisp
  (setq inhibit-compacting-font-caches t)
#+end_src

Performance on Windows is considerably worse than elsewhere, especially if
WSL is involved. We'll need everything we can get.
#+begin_src emacs-lisp
  (when IS-WINDOWS
    (setq w32-get-true-file-attributes nil)) ; slightly faster IO
#+end_src

Adopt a sneaky garbage collection strategy of waiting until idle time to
collect; staving off the collector while the user is working.
#+begin_src emacs-lisp
  (when eden-interactive-mode
    (setq gc-cons-percentage 0.6))
#+end_src

HACK ~tty-run-terminal-initialization~ is *tremendously* slow for some
reason. Disabling it completely could have many side-effects, so we defer it
until later, at which time it (somehow) runs very quickly.
#+begin_src emacs-lisp
  (advice-add #'tty-run-terminal-initialization :override #'ignore)
  (defun eden-init-tty-h ()
    (advice-remove #'tty-run-terminal-initialization #'ignore)
    (tty-run-terminal-initialization (selected-frame) nil t))
  (add-hook 'window-setup-hook #'eden-init-tty-h)
#+end_src

* Run
#+begin_src emacs-lisp
  (defun eden-initialize ()
    "Bootstrap Eden"
    (require 'church-lib)
    (require 'church-package)
    (if noninteractive
	(eden-initialize-packages)
      (eden-load-packages))

    (require 'church-keybinds)
    (require 'church-ui)
    (require 'church-editor)
    (require 'church-modules)
    (require 'church-cli))
#+end_src

* Provides
#+begin_src emacs-lisp
    (provide 'church)
#+end_src
