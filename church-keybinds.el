(when IS-MAC
  (setq mac-command-modifier 'super
        mac-option-modifier 'meta))

(defconst describe-buffer-eden
  "The church is under construction"
  "What `describe-eden' displays in the `help-buffer'")

(defun describe-eden ()
  "Display a buffer showing a short Eden Emacs manual which
points in the right direction for getting further help"
  (interactive)
  (or buffer (setq buffer (current-buffer)))
  (help-setup-xref (list #'describe-eden prefix buffer)
                   (called-interactively-p 'interactive))
  (with-help-window (help-buffer)
    (with-current-buffer (help-buffer)
      (print describe-buffer-eden buffer))))
(global-set-key (kbd "C-h E") #'describe-eden)

(provide 'church-keybinds)
