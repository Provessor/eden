(setq kill-do-not-save-duplicates t)

(setq no-littering-etc-directory eden-etc-dir
      no-littering-var-directory eden-var-dir)
(eden-require 'no-littering)

(provide 'church-editor)
