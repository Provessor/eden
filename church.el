(when (version< emacs-version "27.0.90")
  (error "Detected Emacs %s. Eden only supports Emacs 27.0.90 and higher"
         emacs-version))

(defconst IS-MAC     (eq system-type 'darwin))
(defconst IS-LINUX   (eq system-type 'gnu/linux))
(defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))
(defconst IS-BSD     (or IS-MAC (eq system-type 'berkeley-unix)))

(dolist (sym '(customize-option customize-browse customize-group customize-face
               customize-rogue customize-saved customize-apropos
               customize-changed customize-unsaved customize-variable
               customize-set-value customize-customized customize-set-variable
               customize-apropos-faces customize-save-variable
               customize-apropos-groups customize-apropos-options
               customize-changed-options customize-save-customized))
  (put sym 'disabled "Eden doesn't support `customize', configure Emacs from $EMACSDIR/config.el instead"))
(put 'customize-themes 'disabled "Set `load-theme' in $EMACSDIR/config.el instead")

(add-to-list 'load-path (file-name-directory load-file-name))

(defvar eden--initial-load-path load-path)
(defvar eden--initial-process-environment process-environment)
(defvar eden--initial-exec-path exec-path)

(unless noninteractive
  (defvar eden--initial-file-name-handler-alist file-name-handler-alist)
  (setq file-name-handler-alist nil)

  ;; Restoration
  (defun eden-reset-file-handler-alist-h ()
    (setq file-name-handler-alist eden--initial-file-name-handler-alist))
  (add-hook 'emacs-startup-hook #'eden-reset-file-handler-alist-h))

(defvar eden-debug-mode (or (getenv "DEBUG") init-file-debug)
  "If non-nil, Eden will log more.

Use `eden/toggle-debug-mode' to toggle it. The --debug-init flag and setting the
DEBUG envvar will enable this at startup.")

(defvar eden-interactive-mode (not noninteractive)
  "If non-nil, Emacs is in interactive mode.")

(define-error 'eden-error "Error in Eden Emacs core")
(define-error 'eden-hook-error "Error in a Eden startup hook" 'eden-error)
(define-error 'eden-autoload-error "Error in an autoloads file" 'eden-error)
(define-error 'eden-module-error "Error in a Eden module" 'eden-error)
(define-error 'eden-private-error "Error in private config" 'eden-error)
(define-error 'eden-package-error "Error with packages" 'eden-error)

(setq message-log-max 8192)

(setq debug-on-error eden-debug-mode
      jka-compr-verbose eden-debug-mode)

(setq ad-redefinition-action 'accept)

(defconst eden-emacs-dir
  (eval-when-compile (file-truename user-emacs-directory))
  "The path to the currently loaded Emacs configuration
  directory")

(defconst eden-local-dir (concat eden-emacs-dir ".local/")
  "Root directory for local storage

Use this as a storage location for this system's installation of Doom Emacs.
These files should not be shared across systems. By default, it is used by
`doom-etc-dir' and `doom-cache-dir'. Must end with a slash.")

(defconst eden-etc-dir (concat eden-local-dir "etc/")
  "Directory for non-volatile local storage.

Use this for files that don't change much, like server binaries, external
dependencies or long-term shared data. Must end with a slash.")

(defconst eden-var-dir (concat eden-local-dir "var/")
  "Directory for volatile local storage.

Use this for files that change often, like cache files. Must end with a slash.")

(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))
(prefer-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)

(unless IS-WINDOWS
  (setq selection-coding-system 'utf-8)) ; with sugar on top

(setq gnutls-verify-error (not (getenv "INSECURE"))
      gnutls-algorithm-priority
      (when (boundp 'libgnutls-version)
        (concat "SECURE128:+SECURE192:-VERS-ALL:+VERS-TLS1.2"
                (if (and (not IS-WINDOWS)
                         (not (version< emacs-version "26.3"))
                         (>= libgnutls-version 30605))
                    ":+VERS-TLS1.3")))
      ;; `gnutls-min-prime-bits' is set based on recommendations from
      ;; https://www.keylength.com/en/4/
      gnutls-min-prime-bits 3072
      tls-checktrust gnutls-verify-error
      ;; Emacs is built with `gnutls' by default, so `tls-program' would not
      ;; be used in that case. Otherwiese, people have reasons to not go with
      ;; `gnutls', we use `openssl' instead.
      ;; For more details, see https://redd.it/8sykl1
      tls-program '("openssl s_client -connect %h:%p -CAfile %t -nbio -no_ssl3 -no_tls1 -no_tls1_1 -ign_eof"
                    "gnutls-cli -p %p --dh-bits=3072 --ocsp --x509cafile=%t \
--strict-tofu --priority='SECURE192:+SECURE128:-VERS-ALL:+VERS-TLS1.2:+VERS-TLS1.3' %h"
                    ;; compatibility fallbacks
                    "gnutls-cli -p %p %h"))

(setq auth-sources (list (expand-file-name "authinfo.gpg" eden-etc-dir)
                         "~/.authinfo.gpg"))

(setq ffap-machine-p-known 'reject)

(setq apropos-do-all t)

(unless (daemonp)
  (advice-add #'display-startup-echo-area-message :override #'ignore))

(when IS-WINDOWS
  (setq abbreviated-home-dir "\\`'"))

(setq delete-by-moving-to-trash IS-MAC)

(unless IS-MAC   (setq command-line-ns-option-alist nil))
(unless IS-LINUX (setq command-line-x-option-alist nil))

(setq auto-mode-case-fold nil)

(setq idle-update-delay 1)

(setq-default bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right)

(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)

(setq fast-but-imprecise-scrolling t)

(setq inhibit-compacting-font-caches t)

(when IS-WINDOWS
  (setq w32-get-true-file-attributes nil)) ; slightly faster IO

(when eden-interactive-mode
  (setq gc-cons-percentage 0.6))

(advice-add #'tty-run-terminal-initialization :override #'ignore)
(defun eden-init-tty-h ()
  (advice-remove #'tty-run-terminal-initialization #'ignore)
  (tty-run-terminal-initialization (selected-frame) nil t))
(add-hook 'window-setup-hook #'eden-init-tty-h)

(defun eden-initialize ()
  "Bootstrap Eden"
  (require 'church-lib)
  (require 'church-package)
  (if noninteractive
      (eden-initialize-packages)
    (eden-load-packages))

  (require 'church-keybinds)
  (require 'church-ui)
  (require 'church-editor)
  (require 'church-modules)
  (require 'church-cli))

(provide 'church)
