;;; init.el -*- lexical-binding: t; -*-
;;
;;; License: MIT

;; In noninteractive sessions, prioritize non-byte-compiled source files to
;; prevent the use of stale byte-code. Otherwise, it saves us a little IO time
;; to skip the mtime checks on every *.elc file.
(setq load-prefer-newer noninteractive)

(let (file-name-handler-alist)
  ;; Ensure Doom is running out of this file's directory
  (setq user-emacs-directory (file-name-directory load-file-name)))

;; Load the Church of Eden Emacs
(load (concat user-emacs-directory "church/church")
      nil 'nomessage)

;; Let the session begin
(eden-initialize)
(if noninteractive
    (eden-initialize-packages)
  (eden-initialize-core)
  (eden-initialize-modules))
